<?php
 //if
$i=10;
$j=5;
if($i>=10){
    echo "Pass";
}
echo "<br>";
if($i>$j){
    echo "i is greater than j";
}
echo "<br>";
//if... else
$i= 30;
if($i>33){
    echo "pass";
}
else
    echo "fail";
echo "<br>";
// if..elseif..else
$i=10;
$j=20;
if($i>$j){
    echo "i is greater than j";
}
elseif ($i==$j){
    echo "i is equal to j ";
}
else
{
   echo "i is smaller than j";
}
echo "<br>";
//switch
$i=10;
switch($i){
    case 0:
echo "i is 0";
break;
    case 1:
        echo "i is 1";
        break;
    case 2:
        echo "i is 2";
        break;
    default :
        echo "i is not 0,1,2";

}
echo "<br>";
//while
$i=1;
while($i<=10){
    echo $i++;
    echo "<br>";
}
echo "<br>";
$i=10;
while($i>=0){
    echo $i--;
    echo "<br>";
}
echo "<br>";
//for
for ($i=0;$i<=15;$i++){
    echo "$i";
    echo "<br>";
}
echo "<br>";
//for each
$colors= array("red","green","blue");
foreach ($colors as $key=>$value){
    echo "$key";
    echo "<br>";
    echo "$value <br>";
}

